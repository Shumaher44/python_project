from rules import *
from functions import *

choice = None
f = open('words.txt', 'r')
UNCHANGEBLE_WORDS = []

for i in f:
	UNCHANGEBLE_WORDS.append(f.read())
f.close()
# list_of_word = open("words", "r")
#Программа будет работать до тех пор, пока пользователь не выберит пункт номер 4
while choice != 4:
	print('-----------------------------------------')
	print('Выберите пункт меню:')
	print('\tПросклонять слово - 1')
	print('\tПополнить состав несклоняемых слов - 2')
	print('\tВвысти список не склоняемых слов - 3')
	print('\tВыход - 4')
	print()

	choice = int(input('Введите номер пункта: '))

	if choice == 4: #Данный ввод завершит работу программы
		print('---------------')
		print('Всего доброго!')
			
	elif choice == 1:
		word = input('Введите имя  существительное: ').lower() #В этом блоке пользователь вводит слово 
		if word in UNCHANGEBLE_WORDS[0]:					   #и затем получает результат его склонения
			print('-----------------------')
			print('Это слово не склоняется.\n')
		else:
			word_type(word)
	elif choice == 2:		#В этом блоке пользователь вносит новое слово в словарь с несклоняемыми словами
		add_word(UNCHANGEBLE_WORDS)
	elif choice == 3:
		for i in range(len(UNCHANGEBLE_WORDS)):
			print(UNCHANGEBLE_WORDS[i], ', ')
