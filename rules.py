def word_type(word):
	a = int(input("Укажите склонение имени существительного (1, 2 или3): "))
	if a == 1:
		gender = input('Укажите род имени существительного (м или ж): ')
		if gender == 'м':
			for i in range(6):
				if i == 0:
					new_word = word[:]
					print('Именительный падеж слова', word.title(), '-', new_word)
				elif i == 1:
					new_word = word[:-1] + 'у'
					print('Винительный падеж слова', word.title(), '-', new_word)
				elif i == 2:						
					new_word = word[:-1] + 'и'
					print('Родительный падеж слова', word.title(), '-', new_word)
				elif i == 3:
					new_word = word[:-1] + 'е'
					print('Дательный падеж слова', word.title(), '-', new_word)
				elif i == 4:
					new_word = word[:-1] + 'ой'
					print('Творительный падеж слова', word.title(), '-', new_word)
				elif i == 5:
					new_word = word[:-1] + 'е'
					print('Предложный падеж слова', word.title(), '-', new_word)
				print()
		else:
			for i in range(6):
				if i == 0:
					new_word = word[:]
					print('Именительный падеж слова', word.title(), '-', new_word)
				elif i == 1:
					new_word = word[:-1] + 'ю'
					print('Винительный падеж слова', word.title(), '-', new_word)
				elif i == 2:
					new_word = word[:-1] + 'и'
					print('Родительный падеж слова', word.title(), '-', new_word)
				elif i == 3:
					new_word = word[:-1] + 'е'
					print('Дательный падеж слова', word.title(), '-', new_word)
				elif i == 4:
					new_word = word[:-1] + 'ей'
					print('Творительный падеж слова', word.title(), '-', new_word)
				elif i == 5:
					new_word = word[:-1] + 'е'
					print('Предложный падеж слова', word.title(), '-', new_word)
				print()
	elif a == 2:
		gender = input('Укажите род имени существительного (м или ср): ')
		if gender == 'м':
			if len(word) > 3:
				for i in range(6):
					if i == 0:
						null_end = ' '
						new_word = word[:]
						print('Именительный падеж слова', word.title(), '-', new_word)
					elif i == 1:
						new_word = word[:-1] + 'о'
						print('Винительный падеж слова', word.title(), '-', new_word)
					elif i == 2:
						new_word = word[:-1] + 'а'
						print('Родительный падеж слова', word.title(), '-', new_word)
					elif i == 3:
						new_word = word[:-1] + 'у'
						print('Дательный падеж слова', word.title(), '-', new_word)
					elif i == 4:
						new_word = word[:-1] + 'ом'
						print('Творительный падеж слова', word.title(), '-', new_word)
					elif i == 5:
						new_word = word[:-1] + 'е'
						print('Предложный падеж слова', word.title(), '-', new_word)
						print()
			elif len(word) <= 3:
				for i in range(6):
					if i == 0:
						new_word = word[:]
						print('Именительный падеж слова', word.title(), '-', new_word)
					elif i == 1:
						new_word = word[:] + 'а'
						print('Винительный падеж слова', word.title(), '-', new_word)
					elif i == 2:
						new_word = word[:] + 'а'
						print('Родительный падеж слова', word.title(), '-', new_word)
					elif i == 3:
						new_word = word[:] + 'у'
						print('Дательный падеж слова', word.title(), '-', new_word)
					elif i == 4:
						new_word = word[:] + 'ом'
						print('Творительный падеж слова', word.title(), '-', new_word)
					elif i == 5:
						new_word = word[:] + 'е'
						print('Предложный падеж слова', word.title(), '-', new_word)
						print()
		else:
			for i in range(6):
					if i == 0:
						null_end = ' '
						new_word = word[:]
						print('Именительный падеж слова', word.title(), '-', new_word)
					elif i == 1:
						new_word = word[:-1] + 'о'
						print('Винительный падеж слова', word.title(), '-', new_word)
					elif i == 2:
						new_word = word[:-1] + 'а'
						print('Родительный падеж слова', word.title(), '-', new_word)
					elif i == 3:
						new_word = word[:-1] + 'у'
						print('Дательный падеж слова', word.title(), '-', new_word)
					elif i == 4:
						new_word = word[:-1] + 'ом'
						print('Творительный падеж слова', word.title(), '-', new_word)
					elif i == 5:
						new_word = word[:-1] + 'е'
						print('Предложный падеж слова', word.title(), '-', new_word)
						print()			
	elif a == 3:
		gender = 'ж'
		if gender == 'ж':
			if len(word) > 3:
				for i in range(6):
					if i == 0:
						null_end = ' '
						new_word = word[:]
						print('Именительный падеж слова', word.title(), '-', new_word)
					elif i == 1:
						new_word = word[:] 
						print('Винительный падеж слова', word.title(), '-', new_word)
					elif i == 2:
						new_word = word[:-1] + 'и'
						print('Родительный падеж слова', word.title(), '-', new_word)
					elif i == 3:
						new_word = word[:-1] + 'и'
						print('Дательный падеж слова', word.title(), '-', new_word)
					elif i == 4:
						new_word = word[:] + 'ю'
						print('Творительный падеж слова', word.title(), '-', new_word)
					elif i == 5:
						new_word = word[:-1] + 'и'
						print('Предложный падеж слова', word.title(), '-', new_word)
						print()