from tkinter import *
from functions import *
from rules import *

f = open('words.txt', 'r')

words = []
for i in f:
	words.append(f.read())
f.close()



def clicked():
	if txt.get() == '1':
		lbl.configure(text="Введите слово:")
		word = txt.get()
		if word in words[0]:
			lbl.configure(text="Это слово не склоняется.")
		else:
			word_type(word)
	elif txt.get() == '2':
		add_word(word)
	elif txt.get() == '3':
		for i in range(len(words)):
			print(word[i], ', ')


window = Tk()
window.title('Склонение слов')
lbl = Label(window, text="Введите номер пунтка:\n\tПросклонять слово - 1\n\tПополнить состав несклоняемых слов - 2\n\tВвысти список не склоняемых слов - 3\n\tВыход - 4", font=("Arial Bold", 10))
btn = Button(window, text="Далее", command=clicked)
txt = Entry(window, width=25)
lbl.grid(column=0 , row = 0)
txt.grid(column=0, row = 1)
btn.grid(column=0, row=6)
window.geometry('400x250')
window.mainloop()